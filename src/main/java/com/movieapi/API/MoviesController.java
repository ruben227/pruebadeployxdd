package com.movieapi.API;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import com.movieapi.Model.Movie;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MoviesController {
    private static String des = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tortor est, ultrices ac ligula sit amet, elementum pulvinar libero. Nulla faucibus lectus in imperdiet commodo. Curabitur tristique, est id porta scelerisque, ipsum lorem facilisis libero, in eleifend est neque vel sem. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam id tellus id quam viverra pellentesque. Nunc viverra sit amet augue ut tincidunt. In semper, nunc ut volutpat aliquet, mi massa dignissim nulla, sit amet accumsan enim lacus ut purus. Proin non dolor ante. Nullam vulputate orci in metus tristique, in cursus ante iaculis. In feugiat, metus vel gravida tristique.";
    private static ArrayList<Movie> movies = new ArrayList<Movie>(
            Arrays.asList(new Movie(1, "Downhill", 2015, "https://m.media-amazon.com/images/M/MV5BM2QxMWFlOTktNWY0Mi00MDQ5LWExMTYtNThjZTFhNGZkZjk1XkEyXkFqcGdeQXVyMTkxNjUyNQ@@._V1_UX182_CR0,0,182,268_AL_.jpg",des,"Jose Antonio Tudela","Spain","25/8/2015","Netflix"),
            new Movie(2, "Rifkin's Festival", 2020, "https://m.media-amazon.com/images/M/MV5BNjMzMzk2YjEtZjkwYy00Zjc5LWJmZDctZTY1NTBkNjg3ZDk5XkEyXkFqcGdeQXVyMzI2ODkyNDg@._V1_UY268_CR3,0,182,268_AL_.jpg",des,"Carme Tovar","Spain","19/11/2020","Paramount Pictures"),
            new Movie(3, "Rebeca", 1990, "https://m.media-amazon.com/images/M/MV5BYzgzNGFkNDItZTJkZC00NzJkLTk2OWMtNzliM2I5YzQxYjkxXkEyXkFqcGdeQXVyMTkxNjUyNQ@@._V1_UX182_CR0,0,182,268_AL_.jpg",des,"George Domingo","Spain","25/8/1990","	Universal Pictures"),
            new Movie(4, "Richard Jewell", 2020, "https://m.media-amazon.com/images/M/MV5BOTFlODg1MTEtZTJhOC00OTY1LWE0YzctZjRlODdkYWY5ZDM4XkEyXkFqcGdeQXVyNjU1NzU3MzE@._V1_UX182_CR0,0,182,268_AL_.jpg",des,"Pilar De-Las-Heras","Spain","25/8/2020","Netflix"),
            new Movie(5, "Cadena perpetua", 1994, "https://m.media-amazon.com/images/M/MV5BMDFkYTc0MGEtZmNhMC00ZDIzLWFmNTEtODM1ZmRlYWMwMWFmXkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_UX182_CR0,0,182,268_AL_.jpg",des,"Jose Angel Zambrano","Spain","25/8/1994","Netflix"),
            new Movie(6, "Pulp Fiction", 1990, "https://m.media-amazon.com/images/M/MV5BNGNhMDIzZTUtNTBlZi00MTRlLWFjM2ItYzViMjE3YzI5MjljXkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_UY268_CR1,0,182,268_AL_.jpg",des,"Luz Marina Cantos","Spain","25/8/1990","Amazon"),
            new Movie(7, "Casablanca", 1942, "https://m.media-amazon.com/images/M/MV5BY2IzZGY2YmEtYzljNS00NTM5LTgwMzUtMzM1NjQ4NGI0OTk0XkEyXkFqcGdeQXVyNDYyMDk5MTU@._V1_UX182_CR0,0,182,268_AL_.jpg",des,"Jose Angel Zambrano","Spain","25/8/1942","Netflix"),
            new Movie(8, "El padrino: Parte II", 1974, "https://m.media-amazon.com/images/M/MV5BMWMwMGQzZTItY2JlNC00OWZiLWIyMDctNDk2ZDQ2YjRjMWQ0XkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_UY268_CR3,0,182,268_AL_.jpg",des,"Luisa Ratcliffe","Spain","25/8/1974","Walt Disney Pictures"),
            new Movie(9, "American Beauty", 2020, "https://m.media-amazon.com/images/M/MV5BNTBmZWJkNjctNDhiNC00MGE2LWEwOTctZTk5OGVhMWMyNmVhXkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_UX182_CR0,0,182,268_AL_.jpg",des,"Diogo Gardner","Spain","25/8/2020","Netflix"),
            new Movie(10, "Jurassic Park", 1993, "https://m.media-amazon.com/images/M/MV5BMjM2MDgxMDg0Nl5BMl5BanBnXkFtZTgwNTM2OTM5NDE@._V1_UX182_CR0,0,182,268_AL_.jpg",des,"Kayne Kent","Spain","25/8/1993","	Universal Pictures"),
            new Movie(11, "La naranja mecánica", 1971, "https://m.media-amazon.com/images/M/MV5BMTY3MjM1Mzc4N15BMl5BanBnXkFtZTgwODM0NzAxMDE@._V1_UX182_CR0,0,182,268_AL_.jpg",des,"Irene Davenport","Spain","25/8/1971","Netflix"),
            new Movie(12, "Blade Runner", 1982, "https://m.media-amazon.com/images/M/MV5BNzA1Njg4NzYxOV5BMl5BanBnXkFtZTgwODk5NjU3MzI@._V1_UX182_CR0,0,182,268_AL_.jpg",des,"Vlad Daniel","Spain","25/8/1982","Netflix"),
            new Movie(13, "Chinatown", 2020, "https://m.media-amazon.com/images/M/MV5BOGMwYmY5ZmEtMzY1Yi00OWJiLTk1Y2MtMzI2MjBhYmZkNTQ0XkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_UX182_CR0,0,182,268_AL_.jpg",des,"Abdirahman Bender","Spain","25/8/2020","Columbia Pictures")));

    @GetMapping(value = { "/v1/movies" })
    public List<Movie> Movies(@RequestParam(required = false) String title,
            @RequestParam(required = false) Integer year) {
      return FindByTitleAndYear(title, year);

    }

    @PostMapping("/v1/movies")
    public Movie Add(@RequestBody Movie newMovie) {
        if (FindById(newMovie.getId()) == null) {
            movies.add(newMovie);
            return newMovie;
        }
        return null;
    }

    @DeleteMapping("/v1/movies/{id}")
    public Movie Delete(@PathVariable("id") int id) {

        movies.remove(FindById(id));
        return null;

    }

    @PutMapping("v1/movies/{id}")
    public Movie Update(@RequestBody Movie updateMovie, @PathVariable("id") int id) {

        if (FindById(id) == null)
            return null;

        if (updateMovie.getId() != 0)
            FindById(id).setId(updateMovie.getId());

        if (updateMovie.getTitle() != null)
            FindById(id).setTitle(updateMovie.getTitle());

        if (updateMovie.getYear() != 0)
            FindById(id).setYear(updateMovie.getYear());

        if (updateMovie.getPoster() != null)
            FindById(id).setPoster(updateMovie.getPoster());

        return updateMovie;

    }
    //método que devuelve un string con los mensajes según el idioma
    @GetMapping("/movies/lenguaje")
    public String lenguaje(Locale locale) {
        var messages = ResourceBundle.getBundle("i18n\\messages", locale);

        return messages.getString("Main.Hello") +" " + messages.getString("Main.Movie");
    }
    public static ArrayList<Movie> FindByTitleAndYear(String title, Integer year){
        if (title != null && year != null)
        return FindByYear(year, FindByTitle(title));

    if (title != null) {
        return FindByTitle(title);
    }

    if (year != null) {
        return FindByYear(year, movies);
    }

    ArrayList<Movie> result = new ArrayList<>();
    for (Movie m : movies) {
        result.add(m);
    }
    
    return result;

    }
    public static Movie FindById(int id) {
        for (Movie movie : movies) {
            if (movie.getId() == id) {
                return movie;
            }
        }
        return null;
    }
    public static ArrayList<Movie> GetAll(){
        return movies;
    }
   public static ArrayList<Movie> FindByTitle(String title) {
        ArrayList<Movie> result = new ArrayList<>();
        for (Movie movie : movies) {
            if (movie.getTitle().toLowerCase().contains(title.toLowerCase())) {
                result.add(movie);
            }
        }
       
        return result;
    }

   public static ArrayList<Movie> FindByYear(int year, List<Movie> moviess) {
        ArrayList<Movie> list = new ArrayList<>();
        for (Movie movie : moviess) {
            if (movie.getYear() == year) {
                list.add(movie);
            }
        }
        
        return list;
    }

}
