package com.movieapi.Model;

public class Movie {
    public Movie(){

    }
    public Movie(int _id, String _title, int _year, String _poster,String _synopsis, String _director, String _country,  String _fulldate , String _producer){
        this.id = _id;
        this.year = _year;
        this.title = _title;
        this.poster = _poster;
        this.synopsis = _synopsis;
        this.director = _director;
        this.country = _country;
        this.fulldate = _fulldate;
        this.producer = _producer;
    }
    private int id;
    private String title;
    private int year;
    private String poster;
    private String synopsis;
    private String director;
    private String country;
    private String fulldate;
    private String producer;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getSynopsis() {
        return synopsis;
    }

    public void setSynopsis(String synopsis) {
        this.synopsis = synopsis;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getFulldate() {
        return fulldate;
    }

    public void setFulldate(String fulldate) {
        this.fulldate = fulldate;
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

}
